#!/usr/bin/env bash
set -e
FullExecPath=$PWD
pushd `dirname $0` > /dev/null
FullScriptPath=`pwd`
popd > /dev/null

MySystem=`uname -s`

echo "Building SEfile lib"
cd ../../SEfile_interface_lib
mkdir -p lib
cd lib
/usr/local/tdesktop/Qt-5.6.2/bin/qmake -o Makefile ../../SEfile_interface_lib/SEfile_interface_lib.pro -spec linux-g++ CONFIG+=debug CONFIG+=qml_debug
make
cp src/libSEfile_interface.a libSEfile_interface.a
echo "Building Telegram makefile"
cd $FullExecPath

cd $FullScriptPath

if [ "$MySystem" == "Linux" ]; then
  ../../Libraries/gyp/gyp --depth=. --generator-output=../.. -Goutput_dir=out Telegram.gyp --format=cmake
  cd ../../out/Debug
  # Add SEfile library to list
  sed -i "s/\(target_link_libraries(Telegram.*\)/\1\n  \"\${CMAKE_CURRENT_LIST_DIR}\/..\/..\/..\/SEfile_interface_lib\/lib\/libSEfile_interface.a\"/g" CMakeLists.txt
  sed -i "s/\(list(APPEND Telegram__include_dirs.*\)/\1\n  \"\${CMAKE_CURRENT_LIST_DIR}\/..\/..\/..\/SEfile_interface_lib\/src\"/g" CMakeLists.txt
  sed -i "s/\(list(APPEND Telegram__include_dirs.*\)/\1\n  \"\${CMAKE_CURRENT_LIST_DIR}\/..\/..\/..\/SEfile_interface_lib\/SEfile\"/g" CMakeLists.txt
  sed -i "s/\(list(APPEND Telegram__include_dirs.*\)/\1\n  \"\${CMAKE_CURRENT_LIST_DIR}\/..\/..\/..\/SEfile_interface_lib\/SEfile\/se3\"/g" CMakeLists.txt
  sed -i "s/\(list(APPEND Telegram__include_dirs.*\)/\1\n  \"\/usr\/local\/tdesktop\/Qt-5.6.2\/include\/QtWidgets\"/g" CMakeLists.txt
  cmake .
  cd ../Release
  sed -i "s/\(target_link_libraries(Telegram.*\)/\1\n  \"\${CMAKE_CURRENT_LIST_DIR}\/..\/..\/..\/SEfile_interface_lib\/lib\/libSEfile_interface.a\"/g" CMakeLists.txt
  sed -i "s/\(list(APPEND Telegram__include_dirs.*\)/\1\n  \"\${CMAKE_CURRENT_LIST_DIR}\/..\/..\/..\/SEfile_interface_lib\/src\"/g" CMakeLists.txt
  sed -i "s/\(list(APPEND Telegram__include_dirs.*\)/\1\n  \"\${CMAKE_CURRENT_LIST_DIR}\/..\/..\/..\/SEfile_interface_lib\/SEfile\"/g" CMakeLists.txt
  sed -i "s/\(list(APPEND Telegram__include_dirs.*\)/\1\n  \"\${CMAKE_CURRENT_LIST_DIR}\/..\/..\/..\/SEfile_interface_lib\/SEfile\/se3\"/g" CMakeLists.txt
  sed -i "s/\(list(APPEND Telegram__include_dirs.*\)/\1\n  \"\/usr\/local\/tdesktop\/Qt-5.6.2\/include\/QtWidgets\"/g" CMakeLists.txt
  cmake .
  cd ../../Telegram/gyp
else
  #gyp --depth=. --generator-output=../.. -Goutput_dir=out Telegram.gyp --format=ninja
  #gyp --depth=. --generator-output=../.. -Goutput_dir=out Telegram.gyp --format=xcode-ninja
  #gyp --depth=. --generator-output=../.. -Goutput_dir=out Telegram.gyp --format=xcode
  # use patched gyp with Xcode project generator
  ../../../Libraries/gyp/gyp --depth=. --generator-output=../.. -Goutput_dir=out Telegram.gyp -Gxcode_upgrade_check_project_version=830 --format=xcode
fi

cd ../..

cd $FullExecPath
exit

